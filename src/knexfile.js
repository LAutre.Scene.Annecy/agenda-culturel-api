import dotenv from 'dotenv';

dotenv.config();

export default {
  development: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST || '127.0.0.1',
      port: process.env.DB_PORT || '3306',
      user: process.env.DB_USER,
      password: process.env.DB_SECRET,
      database: process.env.DB_NAME,
    },
    migrations: {
      tableName: 'migrations',
      directory: 'database/migrations',
    },
    seeds: {
      directory: 'database/seeders',
    },
  },

  production: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST || '127.0.0.1',
      port: process.env.DB_PORT || '3306',
      user: process.env.DB_USER,
      password: process.env.DB_SECRET,
      database: process.env.DB_NAME,
    },
    migrations: {
      tableName: 'migrations',
      directory: 'database/migrations',
    },
  },
};
