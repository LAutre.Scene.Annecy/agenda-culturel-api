import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import xss from 'xss-clean';
import cookie from 'cookie-parser';

import ServerConfig from './config/server-config.js';
import routes from './routes/index.js';

const app = express();

// enable logger
app.use(morgan('dev'));

// set security HTTP headers
app.use(helmet());

// parse cookies
app.use(cookie());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());

// enable cors ** might not be needed yet **
// app.use(cors());
// app.options('*', cors());

// register routes
app.use('/api', routes);

app.listen(ServerConfig.PORT, () => {
  console.log(`Successfully started the server on PORT : ${ServerConfig.PORT}`);
});
