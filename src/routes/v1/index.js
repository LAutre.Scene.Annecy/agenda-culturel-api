import { Router } from 'express';
import * as InfoController from '../../controllers/infoController.js';

const router = Router();

router.get('/info', InfoController.info);

export default router;
