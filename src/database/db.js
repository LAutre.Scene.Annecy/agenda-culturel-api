import knex from 'knex';
import bookshelf from 'bookshelf';
import config from '../../knexfile';

const { NODE_ENV } = process.env;

export const db = knex({
  ...config[NODE_ENV],
  debug: NODE_ENV === 'development',
});

export default bookshelf(db);
